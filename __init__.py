from trytond.pool import Pool
from .health import *
from .wizard import *

def register():
    Pool.register(
        PatientData,
        PatientSESAssessment,
        Appointment,
        module='health_patient_fiuner', type_='model')
    Pool.register(
        OpenPatientDU,
        module='health_patient_fiuner', type_='wizard')
